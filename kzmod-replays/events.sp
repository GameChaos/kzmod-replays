public void OnPluginStart_Events()
{
	HookEvent("player_starttimer", Event_StartTimer, EventHookMode_Post);
	HookEvent("player_starttimer2", Event_StartTimer, EventHookMode_Post);
	HookEvent("player_stoptimer", Event_StopTimer, EventHookMode_Pre);
	HookEvent("player_stoptimer2", Event_StopTimer, EventHookMode_Pre);
	HookEvent("player_timerinvalidated", Event_TimerInvalidated, EventHookMode_Pre);
	HookEvent("player_loadcheckpoint", Event_LoadCheckpoint, EventHookMode_Pre);
	
	HookEvent("player_spawn", Event_PlayerSpawn, EventHookMode_Pre);
	HookEvent("player_team", Event_DisableBotMsgs, EventHookMode_Pre);
	HookEvent("player_disconnect", Event_DisableBotMsgs, EventHookMode_Pre);
	HookEvent("player_connect", Event_DisableBotMsgs, EventHookMode_Pre);
	HookEvent("player_changename", Event_DisableBotMsgs, EventHookMode_Pre);
}

public void Event_StartTimer(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	
	char szCourse[64];
	if (StrEqual(name, "player_starttimer"))
	{
		int courseid = event.GetInt("courseid", -1);
		CourseidToString(courseid, szCourse, sizeof(szCourse));
	}
	else
	{
		event.GetString("coursename", szCourse, sizeof(szCourse));
	}
	
	g_bRecordRun[client] = true;
	g_bTPRun[client] = false;
	
	if (g_alRecording[client] == null)
	{
		g_alRecording[client] = new ArrayList(REPLAY_BLOCK_SIZE);
	}
	else
	{
		g_alRecording[client].Clear();
	}
}

// doesn't call when finishing an invalid time
public void Event_StopTimer(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	
	if (g_bInvalidTimer[client] || IsFakeClient(client))
	{
		return;
	}
	
	char szCourse[64];
	event.GetString("coursename", szCourse, sizeof(szCourse));
	
	int timeInCentiSeconds = event.GetInt("milliseconds", -1) + RoundFloat(event.GetFloat("time", -1.0)) * 100;
	
	bool endurance = event.GetBool("endurancecourse");
	SaveRecording(client, szCourse, strlen(szCourse), timeInCentiSeconds, endurance);
}

public void Event_TimerInvalidated(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	
	g_bInvalidTimer[client] = true;
}

public void Event_LoadCheckpoint(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	
	g_bTPRun[client] = true;
}

public Action Event_PlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	
	if (IsFakeClient(client))
	{
		dontBroadcast = true;
		if (g_iReplayBot == -1)
		{
			g_iReplayBot = client;
		}
		else if (client != g_iReplayBot)
		{
			KickClient(client);
		}
		
		if (!g_bPlayReplay)
		{
			SetDefaultBotName(client);
			MoveToSpec(client);
		}
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action Event_DisableBotMsgs(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	
	if (IsValidClient(client) && IsFakeClient(client))
	{
		return Plugin_Handled;
	}
	return Plugin_Continue;
}