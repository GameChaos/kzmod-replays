//
// stocks for the kzmod replays plugin
//

#if defined _kzmod_replays_included
	#endinput
#endif
#define _kzmod_replays_included

stock bool TraceFilter_IgnorePlayer(int ent, int mask, any ignore_me)
{
    return (ent != ignore_me);
}

stock bool IsValidClient(int client)
{
	return (client > 0 && client <= MaxClients && IsValidEntity(client) && IsClientConnected(client) && IsClientInGame(client));
}

void FormatRuntime(int timeInCentiseconds, char[] szFormattedTime, int size)
{
	if (timeInCentiseconds <= 0)
	{
		FormatEx(szFormattedTime, size, "(no time)");
		return;
	}
	
	int time = timeInCentiseconds;
	int iHours = time / 360000;
	int iMinutes = time / 6000 - iHours * 6000;
	int iSeconds = (time - iHours * 360000 - iMinutes * 6000) / 100;
	int iCentiSeconds = time % 100;
	
	if (iHours != 0)
	{
		FormatEx(szFormattedTime, size, "%02i:", iHours);
	}
	if (iMinutes != 0)
	{
		Format(szFormattedTime, size, "%s%02i:", szFormattedTime, iMinutes);
	}
	
	Format(szFormattedTime, size, "%s%02i.%02i", szFormattedTime, iSeconds, iCentiSeconds);
}

stock bool CreateDirectories(const char[] path, int mode, bool use_valve_fs = false, const char[] valve_path_id = "DEFAULT_WRITE_PATH")
{
	char partialPathBuffer[PLATFORM_MAX_PATH];
	
	int currentSplit = 0;
	while (currentSplit < strlen(path))
	{
		int subSplit = FindCharInString(path[currentSplit], '/', false);
		
		if (subSplit == -1) {
			// subsplit the remaining portion of the string
			subSplit = strlen(path[currentSplit]);
		}
		
		currentSplit += subSplit + 1; // subsplit + forward slash
		strcopy(partialPathBuffer, currentSplit + 1, path); // currentsplit + null
		
		if (!DirExists(partialPathBuffer, use_valve_fs, valve_path_id)
				&& !CreateDirectory(partialPathBuffer, mode, use_valve_fs, valve_path_id)) {
			return false;
		}
	}
	return true;
}

stock bool IsValidMoveType(int client)
{
	MoveType moveType = GetEntityMoveType(client);
	
	if (moveType == MOVETYPE_NOCLIP || moveType == MOVETYPE_OBSERVER)
	{
		return false;
	}
	
	if (moveType == MOVETYPE_FLY || moveType == MOVETYPE_FLYGRAVITY)
	{
		return false;
	}
	
	return true;
}