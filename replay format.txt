header:
Int32(REPLAY_MAGIC_NUMBER);
Int8(REPLAY_FMT_VERSION);
Int8(strlen(g_szMap));
String(g_szMap, false);
Int8(courseLen);
String(szCourse, false);
Int32(teleportsUsed);
Int32(GetSteamAccountID(client));
Int8(strlen(steamID2));
String(steamID2, false);
Int8(strlen(alias));
String(alias, false);
Int32(arrayLength);
Int32(GetTime()); // unix 32 bit time
Int32(timeInCentiSeconds);

replay data:
8 cells
32 * 8 bytes = 256 bytes per frame
x, y, z, pitch, yaw, roll, buttons, flags